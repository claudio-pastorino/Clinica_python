# File: rsphere.py Fibonacci numbers

""" Module rnormals: Generates random normals."""

import numpy as np
def rnorm():
	theta=2*np.pi*np.random.uniform()
	phi=np.arccos(2*np.random.uniform()-1)
	rn=(np.cos(theta)*np.sin(phi), np.sin(theta)*np.sin(phi), np.cos(phi))
	return rn

################# MAIN #################

""" Generates random vectors in sphere of radius 10 """
if __name__ == "__main__":
	rc=[]
	import matplotlib.pyplot as plt
	for i in range(10001):
		rc.append(np.array(rnorm())*np.random.uniform(0,10))
	rc=np.array(rc)	
	x=rc[:,0]
	y=rc[:,1]
	z=rc[:,2]
	from mpl_toolkits.mplot3d import Axes3D
	fig=plt.figure()
	ax=fig.add_subplot(111, projection='3d')
	ax.scatter(x,y,z)
	plt.show()
