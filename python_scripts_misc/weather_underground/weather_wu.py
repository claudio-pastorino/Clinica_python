#! /usr/bin/python

import urllib2 
import json
import time
import datetime

f = urllib2.urlopen('http://api.wunderground.com/api/49f8fb752cc8bf74/geolookup/conditions/q/Argentina/Buenos_Aires.json')
json_string = f.read() 
#print json_string
parsed_json = json.loads(json_string) 
f.close()

temp_c = parsed_json['current_observation']['temp_c'] 
hum = parsed_json['current_observation']['relative_humidity'] 
pres = parsed_json['current_observation']['pressure_mb'] 
rocio = parsed_json['current_observation']['dewpoint_c'] 
lluvia = parsed_json['current_observation']['precip_today_metric'] 

# To terminal
#print "Temp: %gC. Humedad: %s. Presion: %s mb "%(temp_c,hum,pres) 
#print " Punto de Rocio: %sC. Precip.: %s "%(rocio,lluvia)

# Fix humidity to normal value

hum=hum.split('%')[0]

# time 
ya=datetime.datetime.now()
rnow=ya.strftime('%Y-%m-%d %H:%M:00')
# Write file 

fw= open('weather.dat','append')
fw.write('%s\t%s\t%s\t%s\t%s\n ' % (str(rnow),str(temp_c),str(hum),str(pres),str(lluvia)))
fw.close()
