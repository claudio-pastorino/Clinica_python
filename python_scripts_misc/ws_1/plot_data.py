#!/usr/bin/python
# funciona !

import plotly
py = plotly.plotly(username='pastorin', key='4l5rct619d')


def pplot(x,y,title,pname): 
    response = py.plot(x,y,
    filename=pname,
    fileopt='overwrite',
    url='https://plot.ly/~pastorin/1',
    layout={'title': title}
    )

def av(l):
    return sum(l)/float(len(l))

# Open and read weather.dat
# Data from weather underground 

fw= open("weather.dat","r")
temp=[]
hum=[]
pres=[]
dia=[]
hora=[]
ptime=[]
lluvia=[]
rocio=[]

c=0
for line in fw:
    val=line.split()
    dia.append(val[0])
    hora.append(val[1])
    ptime.append(val[0]+" "+val[1])
    temp.append(float(val[2]))
    hum.append(float(val[3]))
    pres.append(float(val[4]))
    lluvia.append(float(val[5]))
    rocio.append(float(val[6]))
    c+=1
#funcionaprint(av(temp))

pplot(ptime,pres,'Presion','pres.dat')
pplot(ptime,temp,'Temperatura','temp.dat')
pplot(ptime,hum,'Humedad','hum.dat')
pplot(ptime,lluvia,'Precipitaciones','lluvia.dat')
pplot(ptime,rocio,'Temp. Rocio','rocio.dat')

# 6 hours data 
#last=6*3+1 # 6hours, 3 measurement/hour
#pres6=pres[-last:-1]
#ptime6=ptime[-last:-1]
#hum6=hum[-last:-1]
#temp6=temp[-last:-1]
#lluvia6=lluvia[-last:-1]
#rocio6=rocio[-last:-1]

#pplot(ptime6,pres6,'Presion','pres6.dat')
#pplot(ptime6,temp6,'Temperatura','temp6.dat')
#pplot(ptime6,hum6,'Humedad','hum6.dat')
#pplot(ptime6,lluvia6,'Precipitaciones','lluvia6.dat')
#pplot(ptime6,rocio6,'Temp. Rocio','rocio6.dat')

# 12 hours data 
last=12*3+1 # 12hours, 3 measurement/hour
pres12=pres[-last:-1]
ptime12=ptime[-last:-1]
hum12=hum[-last:-1]
temp12=temp[-last:-1]
lluvia12=lluvia[-last:-1]
rocio12=rocio[-last:-1]

pplot(ptime12,pres12,'Presion','pres12.dat')
pplot(ptime12,temp12,'Temperatura','temp12.dat')
pplot(ptime12,hum12,'Humedad','hum12.dat')
pplot(ptime12,lluvia12,'Precipitaciones','lluvia12.dat')
pplot(ptime12,rocio12,'Temp. Rocio','rocio12.dat')

# 24-hour data 
last=24*3+1 #  24 hours, 3 measurement/hour, 1 extra for python indexing
pres_1d=pres[-last:-1]
ptime_1d=ptime[-last:-1]
hum_1d=hum[-last:-1]
temp_1d=temp[-last:-1]
lluvia_1d=lluvia[-last:-1]
rocio_1d=rocio[-last:-1]

pplot(ptime_1d,pres_1d,'Presion','pres_1d.dat')
pplot(ptime_1d,temp_1d,'Temperatura','temp_1d.dat')
pplot(ptime_1d,hum_1d,'Humedad','hum_1d.dat')
pplot(ptime_1d,lluvia_1d,'Precipitaciones','lluvia_1d.dat')
pplot(ptime_1d,rocio_1d,'Temp. Rocio','rocio_1d.dat')

# four-day data

last=4*24*3+1 #  24 hours, 3 measurement/hour, 1 extra for python indexing
pres_4d=pres[-last:-1]
ptime_4d=ptime[-last:-1]
hum_4d=hum[-last:-1]
temp_4d=temp[-last:-1]
lluvia_4d=lluvia[-last:-1]
rocio_4d=rocio[-last:-1]

pplot(ptime_4d,pres_4d,'Presion','pres_4d.dat')
pplot(ptime_4d,temp_4d,'Temperatura','temp_4d.dat')
pplot(ptime_4d,hum_4d,'Humedad','hum_4d.dat')
pplot(ptime_4d,lluvia_4d,'Precipitaciones','lluvia_4d.dat')
pplot(ptime_4d,rocio_4d,'Temp. Rocio','rocio_4d.dat')

# one week data
last=7*24*3+1 #  24 hours, 3 measurement/hour, 1 extra for python indexing
pres_7d=pres[-last:-1]
ptime_7d=ptime[-last:-1]
hum_7d=hum[-last:-1]
temp_7d=temp[-last:-1]
lluvia_7d=lluvia[-last:-1]
rocio_7d=rocio[-last:-1]

pplot(ptime_7d,pres_7d,'Presion','pres_7d.dat')
pplot(ptime_7d,temp_7d,'Temperatura','temp_7d.dat')
pplot(ptime_7d,hum_7d,'Humedad','hum_7d.dat')
pplot(ptime_7d,lluvia_7d,'Precipitaciones','lluvia_7d.dat')
pplot(ptime_7d,rocio_7d,'Temp. Rocio','rocio_7d.dat')
