#!/usr/bin/python
# funciona !

import plotly
py = plotly.plotly(username='pastorin', key='4l5rct619d')


def pplot(x,y,title,pname): 
    response = py.plot(x,y,
    filename=pname,
    fileopt='overwrite',
    url='https://plot.ly/~pastorin/1',
    layout={'title': title}
    )

def av(l):
    return sum(l)/float(len(l))

# Open and read weather.dat
# Data from weather underground 

fw= open("weather.dat","r")
temp=[]
hum=[]
pres=[]
dia=[]
hora=[]
ptime=[]
lluvia=[]
rocio=[]

c=0
for line in fw:
    val=line.split()
    dia.append(val[0])
    hora.append(val[1])
    ptime.append(val[0]+" "+val[1])
    temp.append(float(val[2]))
    hum.append(float(val[3]))
    pres.append(float(val[4]))
    lluvia.append(float(val[5]))
    rocio.append(float(val[6]))
    c+=1
#funcionaprint(av(temp))

pplot(ptime,temp,'Temperatura','temp.dat')
pplot(ptime,hum,'Humedad','hum.dat')
pplot(ptime,pres,'Presion','pres.dat')
pplot(ptime,lluvia,'Precipitaciones','lluvia.dat')
pplot(ptime,rocio,'Temp. Rocio','rocio.dat')

# 6 hours data 
#last_data=18 # 6hours, 3 measurement/hour
pres6=pres[-1:-]
